#!/bin/bash

# Portable ansible instalation to allow the bootstrap
# https://github.com/ownport/portable-ansible
PORTABLE_ANSIBLE='https://github.com/ownport/portable-ansible/releases/download/ansible-2.5.2/portable-ansible-2.5.2.0.tar.bz2'

# Extract and prepare portable ansible
wget $PORTABLE_ANSIBLE -O ansible.tar.bz2
tar -xjf ansible.tar.bz2
rm ansible.tar.bz2

# Detect the operating system we are running on
case "$OSTYPE" in
  solaris*) OS="SOLARIS" ;;
  darwin*)  OS="OSX" ;; 
  linux*)   OS="LINUX" ;;
  bsd*)     OS="BSD" ;;
  msys*)    OS="WINDOWS" ;;
  *)        OS="unknown: $OSTYPE" ;;
esac
echo "Detected Operating Sytem: $OS"

# Test if ansible is working 
python ansible localhost -m ping

# Make ansible playbook work
ln -s ansible ansible-playbook

# Run the bootstrap.yml playbook
sudo python ansible-playbook -i ./inventory bootstrap.yml 