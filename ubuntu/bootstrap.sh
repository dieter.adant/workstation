#!/bin/bash

# Initial Packages
sudo apt update
sudo apt -y upgrade
sudo apt -y install i3 i3blocks rofi git build-essential python3 compton wine-stable vim curl jq sqlite3

# Cross platform 
sudo apt -y install libc6-armel-cross libc6-dev-armel-cross binutils-arm-linux-gnueabi libncurses5-dev gcc-arm-linux-gnueabihf

# Java 8
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get -y install oracle-java8-installer

# python 
sudo apt -y install python-pip

# Docker
sudo snap install docker

# Visual Studio code
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt-get update
sudo apt-get install code

# Liquorix Kernel
sudo add-apt-repository ppa:damentz/liquorix && sudo apt-get update
sudo apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64
# 32-bit with PAE (HIGHMEM64G):
# sudo apt-get install linux-image-liquorix-686-pae linux-headers-liquorix-686-pae
# 32-bit without PAE (HIGHMEM4G)
# sudo apt-get install linux-image-liquorix-686 linux-headers-liquorix-686

# Cleanup Packages
sudo apt autoremove


