# How to run boxstarter 

# 1. Make sure boxstarter is installed  
# @todo: way to automate this in one step ?

# Disbale execution
# Set-ExecutionPolicy RemoteSigned

# powershell v3
# iex ((New-Object System.Net.WebClient).DownloadString('https://boxstarter.org/bootstrapper.ps1')); get-boxstarter -Force
# powershell v2
#. { iwr -useb http://boxstarter.org/bootstrapper.ps1 } | iex; get-boxstarter -Force

# 2. Run this script in a Administrative shell
# @todo: ways to elevate this shell ? 
Install-BoxstarterPackage -PackageName bootsrap.ps1  -DisableReboots