#!/bin/bash
function get_cloudops_projects(){
    curl -L -s -u $collibra_ldap_user:$collibra_ldap_password $collibra_bitbucket_server/rest/api/1.0/projects/CLOUD/repos | jq -r '.values[].links.clone[].href' | grep 'ssh://'
}

function init_git_repos(){
    for git in $(get_cloudops_projects)
    do
        git clone $git
    done
}

read -p "Are you sure, you want to initalize collibra cloudops repos in $PWD (y\n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    $(init_git_repos)
fi
